module MetasploitHuntDown

go 1.21

require (
	github.com/Netflix/go-expect v0.0.0-20220104043353-73e0943537d2 // indirect
	github.com/creack/pty v1.1.17 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/hinshun/vt10x v0.0.0-20220301184237-5011da428d02 // indirect
	github.com/kr/pty v1.1.8 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/protobuf v1.26.0 // indirect
	gopkg.in/vmihailenco/msgpack.v2 v2.9.2 // indirect
)
