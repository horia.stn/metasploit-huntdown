package rpc

import (
	"MetasploitHuntDown/actions"
	"fmt"
	"log"
	"strings"
)

type ModuleOption struct {
	Title        string `json:"title"`
	MainGroup    string `json:"maingroup"`
	SubGroup     string `json:"subgroup"`
	Flag         string `json:"flag"`
	Value        string `json:"value"`
	NeedCheckbox bool   `json:"needcheckbox"`
	Checkbox     bool   `json:"checkbox"`
	TextInput    bool   `json:"textinput"`
	Information  string `json:"information"`
	Regex        string `json:"regex"`
}

/*
Gets all exlpoits modules available in Metasploit
*/
func (msf *Metasploit) GetAllExploits() ([]string, error) {
	var res moduleExploitsRes
	ctx := &moduleExploitsReq{
		Method: actions.ModuleExploits,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.ExploitType)

	return res.Modules, nil
}

/*
Gets all auxiliary modules available in Metasploit
*/
func (msf *Metasploit) GetAllAuxiliary() ([]string, error) {
	var res moduleAuxiliaryRes
	ctx := &moduleAuxiliaryReq{
		Method: actions.ModuleAuxiliary,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.AuxiliaryType)

	return res.Modules, nil
}

/*
Gets all post modules available in Metasploit
*/
func (msf *Metasploit) GetAllPosts() ([]string, error) {
	var res modulePostRes
	ctx := &modulePostReq{
		Method: actions.ModulePost,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.PostType)

	return res.Modules, nil
}

/*
Gets all payloads modules available in Metasploit
*/
func (msf *Metasploit) GetAllPayloads() ([]string, error) {
	var res modulePayloadsRes
	ctx := &modulePayloadsReq{
		Method: actions.ModulePayloads,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.PayloadType)

	return res.Modules, nil
}

/*
Gets all encoders modules available in Metasploit
*/
func (msf *Metasploit) GetAllEncoders() ([]string, error) {
	var res moduleEncodersRes
	ctx := &moduleEncodersReq{
		Method: actions.ModuleEncoders,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.EncoderType)

	return res.Modules, nil
}

/*
Gets all nops modules available in Metasploit
*/
func (msf *Metasploit) GetAllNops() ([]string, error) {
	var res moduleNopsRes
	ctx := &moduleNopsReq{
		Method: actions.ModuleNops,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	msf.GetAllModulesInfo(res.Modules, actions.NopType)

	return res.Modules, nil
}

/*
Gets more information for a module
*/
func (msf *Metasploit) GetModuleInfo(moduleName string, moduleType string) (moduleInfoRes, error) {
	var res moduleInfoRes
	ctx := &moduleInfoReq{
		Method:     actions.ModuleInfo,
		Token:      msf.Token,
		ModuleType: moduleType,
		ModuleName: moduleName,
	}

	if err := msf.send(ctx, &res); err != nil {
		fmt.Println("MODULE INFO ERR:", err)
		// return res, err
	}

	return res, nil
}

/*
This Function gets all modules names and pull all the details
for each one and stores them as a JSON file

To Save time and not create another struct to add just one field
, because filepath is not needed in any action
I will store the exploit name into that field and use it for further actions
*/
func (msf *Metasploit) GetAllModulesInfo(modules []string, moduleType string) error {
	var moduleInfoSlice []moduleInfoRes

	for _, module := range modules {
		moduleInfo, err := msf.GetModuleInfo(module, moduleType)
		if err != nil {
			fmt.Printf("ERROR - failed to get module info for %s: %v\n", module, err)
			return err
		}

		moduleInfo.FilePath = module

		moduleInfoSlice = append(moduleInfoSlice, moduleInfo)
	}

	err := WriteModulesJSON(moduleInfoSlice, moduleType)
	if err != nil {
		log.Fatalf("Error writing modules to JSON: %v", err)
	}

	return nil
}

/*
This function parses the response into the json format supported by UI
*/
func WriteModulesJSON(modules []moduleInfoRes, moduleType string) error {
	var moduleOptions []ModuleOption

	filename := moduleType + "_modules.json"
	filepath := "modules-json/" + filename

	for _, module := range modules {
		option := ModuleOption{
			Title:        module.Name,        // Example: use module name as title
			MainGroup:    "Modules",          // Example: static value
			SubGroup:     moduleType,         // Example: static value
			Flag:         "",                 // Example: populate as needed
			Value:        module.FilePath,    // Example: populate as needed
			NeedCheckbox: true,               // Example: static value
			Checkbox:     true,               // Example: static value
			TextInput:    false,              // Example: static value
			Information:  module.Description, // Example: use module description
			Regex:        "",                 // Example: populate as needed
		}
		moduleOptions = append(moduleOptions, option)
	}

	// data, err := json.MarshalIndent(moduleOptions, "", "  ")
	// if err != nil {
	// 	log.Printf("Failed to marshal modules to JSON: %v\n", err)
	// 	return err
	// }

	log.Printf("Writing modules to file: %s\n", filepath)
	// err = os.WriteFile(filepath, data, 0644)
	// if err != nil {
	// 	log.Printf("Failed to write modules to file: %v\n", err)
	// 	return err
	// }

	log.Printf("Successfully wrote modules to file: %s\n", filepath)
	return nil
}

/*
This function executes a module on a desired host
*/
func (msf *Metasploit) ExecuteModule(hostAddr string, exploitPath string, exploitType string) (*moduleExecuteRes, error) {

	var moduleExecuteRes moduleExecuteRes

	host := strings.Split(hostAddr, ":")
	ip := host[0]
	port := host[1]

	options := make(map[string]string)
	options["RHOST"] = ip
	options["RPORT"] = port

	ctx := &moduleExecuteReq{
		Method:     actions.ModuleExecute,
		Token:      msf.Token,
		ModuleType: exploitType,
		ModuleName: exploitPath,
		Options:    options,
	}

	if err := msf.send(ctx, &moduleExecuteRes); err != nil {
		fmt.Println(err)
		return nil, err
	}
	fmt.Println(moduleExecuteRes)
	return &moduleExecuteRes, nil
}

func (msf *Metasploit) ExecutePayloadModule(hostAddr string, format string, exploitPath string, exploitType string) (*moduleExecutePayloadRes, error) {
	var moduleExecutePayloadRes moduleExecutePayloadRes

	host := strings.Split(hostAddr, ":")
	ip := host[0]
	port := host[1]

	options := make(map[string]string)
	options["LHOST"] = ip
	options["LPORT"] = port
	options["Format"] = format

	ctx := &moduleExecuteReq{
		Method:     actions.ModuleExecute,
		Token:      msf.Token,
		ModuleType: exploitType,
		ModuleName: exploitPath,
		Options:    options,
	}

	if err := msf.send(ctx, &moduleExecutePayloadRes); err != nil {
		fmt.Println(err)
		return nil, err
	}
	fmt.Println(moduleExecutePayloadRes.Payload)
	return &moduleExecutePayloadRes, nil
}
