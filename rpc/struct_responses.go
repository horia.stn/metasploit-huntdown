package rpc

// Auth Responses
type loginRes struct {
	Result       string `msgpack:"result"`
	Token        string `msgpack:"token"`
	Error        bool   `msgpack:"error"`
	ErrorClass   string `msgpack:"error_class"`
	ErrorMessage string `msgpack:"error_message"`
}
type logoutRes struct {
	Result string `msgpack:"result"`
}

// Session Responses
type sessionWriteRes struct {
	WriteCount string `msgpack:"write_count"`
}
type sessionReadRes struct {
	Seq  uint32 `msgpack:"seq"`
	Data string `msgpack:"data"`
}
type SessionListRes struct {
	ID          uint32 `msgpack:",omitempty"`
	Type        string `msgpack:"type"`
	TunnelLocal string `msgpack:"tunnel_local"`
	TunnelPeer  string `msgpack:"tunnel_peer"`
	ViaExploit  string `msgpack:"via_exploit"`
	ViaPayload  string `msgpack:"via_payload"`
	Description string `msgpack:"desc"`
	Info        string `msgpack:"info"`
	Workspace   string `msgpack:"workspace"`
	SessionHost string `msgpack:"session_host"`
	SessionPort int    `msgpack:"session_port"`
	Username    string `msgpack:"username"`
	UUID        string `msgpack:"uuid"`
	ExploitUUID string `msgpack:"exploit_uuid"`
}
type sessionRingLastRes struct {
	Seq uint32 `msgpack:"seq"`
}

type SessionMeterpreterReadRes struct {
	Data string `msgpack:"data"`
}

type SessionMeterpreterWriteRes struct {
	Result Result `msgpack:"result"`
}

// Console Responses
type ConsoleCreateRes struct {
	Id     string `msgpack:"id"`
	Prompt string `msgpack:"prompt"`
	Busy   bool   `msgpack:"busy"`
}

type ConsoleReadRes struct {
	Data   string `msgpack:"data"`
	Prompt string `msgpack:"prompt"`
	Busy   bool   `msgpack:"busy"`
}

type ConsoleWriteRes struct {
	Wrote int `msgpack:"wrote"`
}

// Module Responses
type moduleExploitsRes struct {
	Modules []string `msgpack:"modules"`
}
type moduleAuxiliaryRes struct {
	Modules []string `msgpack:"modules"`
}

type modulePostRes struct {
	Modules []string `msgpack:"modules"`
}

type modulePayloadsRes struct {
	Modules []string `msgpack:"modules"`
}

type moduleEncodersRes struct {
	Modules []string `msgpack:"modules"`
}

type moduleNopsRes struct {
	Modules []string `msgpack:"modules"`
}

type moduleInfoRes struct {
	Name        string     `msgpack:"name"`
	Description string     `msgpack:"description"`
	License     string     `msgpack:"license"`
	FilePath    string     `msgpack:"filepath"`
	Version     string     `msgpack:"version"`
	Rank        string     `msgpack:"rank"`
	References  [][]string `msgpack:"references"`
	Authors     []string   `msgpack:"authors"`
}

type moduleExecuteRes struct {
	Jid     int    `msgpack:"job_id"`
	UUID    string `msgpack:"uuid"`
	Message string `msgpack:"message,omitempty"`
	Error   string `msgpack:"error,omitempty"`
}

type moduleExecutePayloadRes struct {
	Payload string `msgpack:"payload"`
	JobID   int    `msgpack:"job_id"`
}

// Job responses
type jobInfoResp struct {
	Jid       int                    `msgpack:"jid"`
	Name      string                 `msgpack:"name"`
	StartTime int                    `msgpack:"start_time"`
	Status    string                 `msgpack:"status"`
	Result    string                 `msgpack:"result"`
	Datastore map[string]interface{} `msgpack:"datastore,omitempty"`
}

type JobListResp map[string]string

type jobStopRes struct {
	Result Result `msgpack:"result"`
}
