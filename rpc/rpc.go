package rpc

import (
	"bytes"
	"fmt"
	"net/http"

	"gopkg.in/vmihailenco/msgpack.v2"
)

type Result string

var (
	Failure Result = "failure"
	Success Result = "success"
)

type Metasploit struct {
	Host  string
	User  string
	Pass  string
	Token string
	http  *http.Client
	url   string
}

func New(host, user, pass string, http *http.Client, url string) (*Metasploit, error) {
	msf := &Metasploit{
		Host: host,
		User: user,
		Pass: pass,
		http: http,
		url:  url,
	}

	if err := msf.Login(); err != nil {
		return nil, err
	}

	return msf, nil
}

func (msf *Metasploit) send(req interface{}, res interface{}) error {
	buf := new(bytes.Buffer)
	msgpack.NewEncoder(buf).Encode(req)
	// dest := fmt.Sprintf("http://%s/api", msf.Host)
	// response, err := http.Post(dest, "binary/message-pack", buf)
	response, err := msf.http.Post(msf.url, "binary/message-pack", buf)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	if err := msgpack.NewDecoder(response.Body).Decode(&res); err != nil {
		return err
	}
	return nil
}

func (msf *Metasploit) Login() error {
	ctx := &loginReq{
		Method:   "auth.login",
		Username: msf.User,
		Password: msf.Pass,
	}

	fmt.Println("msf", msf)

	var res loginRes
	if err := msf.send(ctx, &res); err != nil {
		fmt.Println("Failed at login")
		fmt.Println("ERROR", err)
		return err
	}
	msf.Token = res.Token
	fmt.Println("token", msf.Token)
	return nil
}

func (msf *Metasploit) Logout() error {
	ctx := &logoutReq{
		Method:      "auth.logout",
		Token:       msf.Token,
		LogoutToken: msf.Token,
	}

	var res logoutRes
	if err := msf.send(ctx, &res); err != nil {
		return err
	}
	msf.Token = ""
	return nil
}
