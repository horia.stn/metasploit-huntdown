package rpc

import (
	"MetasploitHuntDown/actions"
	"fmt"
)

func (msf *Metasploit) ConsoleRead(ConsoleId string) (*ConsoleReadRes, error) {
	ctx := &ConsoleReadReq{
		Method:    actions.ConsoleRead,
		Token:     msf.Token,
		ConsoleID: ConsoleId,
	}

	var consoleReadRes ConsoleReadRes
	if err := msf.send(ctx, &consoleReadRes); err != nil {
		return nil, err
	}

	return &consoleReadRes, nil
}

func (msf *Metasploit) ConsoleWrite(consoleID string, command string) error {
	ctx := &ConsoleWriteReq{
		Method:    actions.ConsoleWrite,
		Token:     msf.Token,
		ConsoleID: consoleID,
		Command:   command,
	}

	var consoleWriteRes ConsoleWriteRes
	if err := msf.send(ctx, &consoleWriteRes); err != nil {
		return err
	}

	if consoleWriteRes.Wrote == 0 {
		return fmt.Errorf("Error at writing in console , wrong console ID")
	}

	fmt.Println("WRITE SUCCESFULL", consoleWriteRes)

	return nil
}

func (msf *Metasploit) ConsoleCreate() (string, error) {
	ctx := &consoleCreateReq{
		Method: actions.ConsoleCreate,
		Token:  msf.Token,
	}

	var consoleCreateRes ConsoleCreateRes
	if err := msf.send(ctx, &consoleCreateRes); err != nil {
		return "", err
	}

	return consoleCreateRes.Id, nil
}
