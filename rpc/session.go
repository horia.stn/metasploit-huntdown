package rpc

import (
	"MetasploitHuntDown/actions"
	"fmt"
	"time"
)

func (msf *Metasploit) SessionList() (map[uint32]SessionListRes, error) {
	req := &sessionListReq{
		Method: actions.SessionList,
		Token:  msf.Token,
	}

	res := make(map[uint32]SessionListRes)
	if err := msf.send(req, &res); err != nil {
		return nil, err
	}

	for id, session := range res {
		session.ID = id
		res[id] = session
	}
	return res, nil

}

func (msf *Metasploit) SessionReadPointer(session uint32) (uint32, error) {
	ctx := &sessionRingLastReq{
		Method:    "session.ring_last",
		Token:     msf.Token,
		SessionID: session,
	}

	var sesRingLast sessionRingLastRes
	if err := msf.send(ctx, &sesRingLast); err != nil {
		return 0, err
	}

	return sesRingLast.Seq, nil
}

func (msf *Metasploit) SessionWrite(session uint32, command string) error {
	ctx := &sessionWriteReq{
		Method:    "session.shell_write",
		Token:     msf.Token,
		SessionID: session,
		Command:   command,
	}

	var res sessionWriteRes
	if err := msf.send(ctx, &res); err != nil {
		return err
	}

	fmt.Println("sessionWrite", res)

	return nil
}

func (msf *Metasploit) SessionRead(session uint32, readPointer uint32) (string, error) {
	ctx := &sessionReadReq{
		Method:      "session.shell_read",
		Token:       msf.Token,
		SessionID:   session,
		ReadPointer: string(readPointer),
	}

	var res sessionReadRes
	if err := msf.send(ctx, &res); err != nil {
		return "", err
	}

	return res.Data, nil
}

func (msf *Metasploit) SessionMeterpreterWrite(sid int, data string) (*SessionMeterpreterWriteRes, error) {
	ctx := &SessionMeterpreterWriteReq{
		Method: actions.SessionMeterpreterWrite,
		Token:  msf.Token,
		Sid:    sid,
		Data:   data,
	}

	var res *SessionMeterpreterWriteRes
	err := msf.send(ctx, &res)
	if err != nil {
		return nil, err
	}

	fmt.Println("Session Met Write Res:", res)

	return res, nil
}

func (msf *Metasploit) SessionMeterpreterRead(sid int) (*SessionMeterpreterReadRes, error) {
	ctx := SessionMeterpreterReadReq{
		Method: actions.SessionMeterpreterRead,
		Token:  msf.Token,
		Sid:    sid,
	}

	var res *SessionMeterpreterReadRes
	err := msf.send(ctx, &res)
	if err != nil {
		return nil, err
	}

	fmt.Println("Session Met read Res:", res.Data)
	return res, nil
}

func (msf *Metasploit) SessionExecute(session uint32, command string, sessionType string) (string, error) {
	var data string
	if sessionType == "meterpreter" {
		msf.SessionMeterpreterWrite(int(session), command)
		time.Sleep(10 * time.Second)
		sessionMeterpreterRead, err := msf.SessionMeterpreterRead(int(session))
		if err != nil {
			return "", err
		}
		data = sessionMeterpreterRead.Data
	} else {
		readPointer, err := msf.SessionReadPointer(session)
		if err != nil {
			return "", err
		}
		msf.SessionWrite(session, command)
		sessionRead, err := msf.SessionRead(session, readPointer)
		if err != nil {
			return "", err
		}
		data = sessionRead
	}
	return data, nil
}

func (msf *Metasploit) SessionExecuteList(session uint32, commands []string, sessionType string) (string, error) {
	var results string
	for _, command := range commands {
		tCommand := fmt.Sprintf("%s\n", command)
		result, err := msf.SessionExecute(session, tCommand, sessionType)
		if err != nil {
			return results, err
		}
		results += result
	}

	return results, nil
}
