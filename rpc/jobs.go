package rpc

import (
	"MetasploitHuntDown/actions"
)

func (msf *Metasploit) GetJobList() (*JobListResp, error) {
	var res *JobListResp

	ctx := &jobListReq{
		Method: actions.JobList,
		Token:  msf.Token,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (msf *Metasploit) GetJobInfo(jid int) (*jobInfoResp, error) {
	var res *jobInfoResp

	ctx := &jobInfoReq{
		Method: actions.JobInfo,
		Token:  msf.Token,
		Jid:    jid,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}

func (msf *Metasploit) StopJob(jid int) (*jobStopRes, error) {
	var res *jobStopRes

	ctx := &jobStopReq{
		Method: actions.JobStop,
		Token:  msf.Token,
		Jid:    jid,
	}

	if err := msf.send(ctx, &res); err != nil {
		return nil, err
	}

	return res, nil
}
