package msfgocreation

import (
	"MetasploitHuntDown/rpc"
	"crypto/tls"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
)

type MsfClientOptions struct {
	Timeout         time.Duration
	ProxyUrl        string
	TlsClientConfig *tls.Config
	Token           string
	SSL             bool
	ApiVersion      string
}

type UserInfo struct {
	Msfhost string `json:"msfhost"`
	Msfpass string `json:"msfpass"`
}

func NewClient() (*rpc.Metasploit, error) {
	clientOptions := MsfClientOptions{
		Token:           "",
		SSL:             true,
		TlsClientConfig: &tls.Config{InsecureSkipVerify: true},
		ApiVersion:      "1.0",
	}
	userInfo, err := getUserInfo()
	if err != nil {
		return nil, err
	}

	url := createUrl(userInfo.Msfhost, clientOptions.SSL, clientOptions.ApiVersion)

	http, err := newHttpClient(clientOptions)
	if err != nil {
		return nil, err
	}

	rpc, err := rpc.New(userInfo.Msfhost, "msf", userInfo.Msfpass, http, url)
	if err != nil {
		return nil, err
	}

	return rpc, nil
}

func newHttpClient(options MsfClientOptions) (*http.Client, error) {
	transport := http.DefaultTransport.(*http.Transport).Clone()
	transport.TLSClientConfig = options.TlsClientConfig

	if options.ProxyUrl != "" {
		proxyUrl, err := url.Parse(options.ProxyUrl)
		if err != nil {
			return nil, err
		}

		transport.Proxy = http.ProxyURL(proxyUrl)
	}

	return &http.Client{
		Timeout:   options.Timeout,
		Transport: transport,
	}, nil
}

func createUrl(addr string, isSsl bool, apiVersion string) string {
	protocol := "http"
	if isSsl {
		protocol = "https"
	}

	return fmt.Sprintf("%s://%s/api/%s", protocol, addr, apiVersion)
}

func getUserInfo() (UserInfo, error) {
	// jsonFile, err := os.Open("user-info.json")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return UserInfo{}, err
	// }
	// defer jsonFile.Close()
	var userInfo UserInfo
	// decoder := json.NewDecoder(jsonFile)
	// err = decoder.Decode(&userInfo)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return UserInfo{}, err
	// }

	msfhost := os.Getenv("MSFHOST")
	msfpass := os.Getenv("MSFPASS")
	if msfhost == "" || msfpass == "" {
		log.Fatalln("MSFHOST or MSFPASS environment variables are not set")
		return UserInfo{}, fmt.Errorf("MSFHOST or MSFPASS environment variables are not set")
	}

	userInfo.Msfhost = msfhost
	userInfo.Msfpass = msfpass

	return userInfo, nil
}
