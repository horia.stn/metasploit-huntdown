package json_parser

type ModuleOption struct {
	Title        string `json:"title"`
	MainGroup    string `json:"maingroup"`
	SubGroup     string `json:"subgroup"`
	Flag         string `json:"flag"`
	Value        string `json:"value"`
	NeedCheckbox bool   `json:"needcheckbox"`
	Checkbox     bool   `json:"checkbox"`
	TextInput    bool   `json:"textinput"`
	Information  string `json:"information"`
	Regex        string `json:"regex"`
}

type moduleInfoRes struct {
	Name        string     `msgpack:"name"`
	Description string     `msgpack:"description"`
	License     string     `msgpack:"license"`
	FilePath    string     `msgpack:"filepath"`
	Version     string     `msgpack:"version"`
	Rank        string     `msgpack:"rank"`
	References  [][]string `msgpack:"references"`
	Authors     []string   `msgpack:"authors"`
}
