package actions

const (
	JobList string = "job.list"
	JobStop string = "job.stop"
	JobInfo string = "job.info"
)
