package actions

const (
	ExploitType   = "exploit"
	AuxiliaryType = "auxiliary"
	PostType      = "post"
	NopType       = "nop"
	PayloadType   = "payload"
	EncoderType   = "encoder"
)
