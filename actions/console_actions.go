package actions

const (
	ConsoleRead   string = "console.read"
	ConsoleCreate string = "console.create"
	ConsoleWrite  string = "console.write"
)
